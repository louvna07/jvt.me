---
title: Open Source Projects I Maintain
---
There are a few Free and Open Source projects that I maintain:

- [Wiremock](https://github.com/wiremock/wiremock)
- [Spectral Test Harness](https://gitlab.com/jamietanna/spectral-test-harness)
- [Cucumber Reporting Plugin](https://gitlab.com/jamietanna/cucumber-reporting-plugin)
- [RSVP Calendar](https://gitlab.com/jamietanna/rsvp-calendar)
- [Gherkin Formatter](https://gitlab.com/jamietanna/gherkin-formatter)
- [books-mf2](https://gitlab.com/jamietanna/books-mf2)
- [meetup-mf2](https://gitlab.com/jamietanna/meetup-mf2)
- [eventbrite-mf2](https://gitlab.com/jamietanna/eventbrite-mf2)
- [Jenkins job-dsl-plugin](https://github.com/jenkinsci/job-dsl-plugin/)
- [tokens-pls](https://gitlab.com/jamietanna/tokens-pls)
- [indieauth-postman](https://gitlab.com/jamietanna/indieauth-postman)
- [micropub-postman](https://gitlab.com/jamietanna/micropub-postman)
- [jwks-ical](https://gitlab.com/jamietanna/jwks-ical)
- [media-type](https://gitlab.com/jamietanna/media-type)
- [content-negotiation](https://gitlab.com/jamietanna/content-negotiation)
- [multiple-read-servlet](https://gitlab.com/jamietanna/multiple-read-servlet)
- [uuid](https://gitlab.com/jamietanna/uuid)
- [spring-content-negotiator](https://gitlab.com/jamietanna/spring-content-negotiator)
- [openapi.tanna.dev](https://gitlab.com/jamietanna/openapi.tanna.dev)
- [micropub-go](https://gitlab.com/jamietanna/micropub-go/)

I also work very hard on producing Free and Open Source solutions as part of the content on this website.

I also host a number of services:

- [rsvp-calendar.tanna.dev](https://rsvp-calendar.tanna.dev/)
- [books-mf2.herokuapp.com](https://books-mf2.herokuapp.com)
- [meetup-mf2.herokuapp.com](https://meetup-mf2.herokuapp.com)
- [eventbrite-mf2.herokuapp.com](https://eventbrite-mf2.herokuapp.com)
- [tokens-pls.herokuapp.com](https://tokens-pls.herokuapp.com/)
- [openapi.tanna.dev](https://openapi.tanna.dev)
